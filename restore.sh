#!/usr/bin/env bash

# restore.sh
# uses rsync to restore files from the backup drive to the local user accounts

# Display Commands
set -x

# Clear any previous sudo permission
sudo -k

# Check for root
[[ $(id -u) -eq 0 ]] && echo 'script must be run as a normal user' && exit 1

# Set up source and destination variables
BACK="/media/chris/LACIE"
BACK_CHR="${BACK}/chris"
BACK_AND="${BACK}/andrea"

# run rsync commands on wanted directories.
HOME_CHR="/home/chris"
HOME_AND="/home/andrea"

# Note: the trailing slash is required on the source folder so rsync copies its contents
#       INTO the destination folder. Otherwise it will copy the source folder AND
#       its contents into the destination folder

LIST="Desktop Documents Downloads Music Pictures Videos"

for folder in $LIST; do
  sudo rsync -a  "${BACK_CHR}/${folder}/"  "${HOME_CHR}/${folder}"
  sudo rsync -a  "${BACK_AND}/${folder}/"  "${HOME_AND}/${folder}"
done
