#!/usr/bin/env bash

# backup.sh
# uses rsync to backup home folder content to a backup drive

# Display Commands
set -x

# Clear any previous sudo permission
sudo -k

# Check for root
[[ $(id -u) -eq 0 ]] && echo 'script must be run as a normal user' && exit 1

# Set up source and destination variables
BACK="/media/chris/LACIE"
BACK_CHR="${BACK}/chris"
BACK_AND="${BACK}/andrea"

# Set up backup directories (this could be the first run)
mkdir -p $BACK_CHR
mkdir -p $BACK_AND

# run rsync commands on wanted directories.
HOME_CHR="/home/chris"
HOME_AND="/home/andrea"

# Note: the trailing slash is required on the source folder so rsync copies its contents
#       INTO the destination folder. Otherwise it will copy the source folder AND
#       its contents into the destination folder

LIST="Desktop Documents Downloads Music Pictures Videos"

for folder in $LIST; do
  sudo rsync -a --del "${HOME_CHR}/${folder}/"  "${BACK_CHR}/${folder}"
  sudo rsync -a --del "${HOME_AND}/${folder}/"  "${BACK_AND}/${folder}"
done
